
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
//the beginning !!!
$.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
});

// ta koda se ne izvaja
var pacientTest=[{
    "firstNames": "Jordan",
    "lastNames": "Nolan",
    "gender": "MALE",
    "dateOfBirth": "1990-03-09",
    "address": {
      "address": "Toronto, Canada"
    },
    "additionalInfo": {
        "pet": "dog",
        "title": "Mr"
    }
}
];
// ta koda se ne

var pacienti = [{
        "firstNames": "Andres",
        "lastNames": "Gomilov",
        "gender": "MALE",
        "dateOfBirth": "1993-06-11",
        "address": {
            "address": "Street of Hamir Gerbanov, 2nd district 3a"
        },
        "additionalInfo": {
            "graph1" : "162,162.5,164,161",
            "graph2" : "98,99,100,98",
            "graph3" : "6.0,6.1,7.3,6.0",
            "graph4" : "80,82,81,82",
            "graph5" : "92,96,91,93",
            "height" : 162,
            "weight" : 98,
            "temperature" : 41.2,
            "dia_bPressure" : 82,
            "sys_bPressure" : 130,
            "eduCentre" : "Tashkent university of Medicine",
            "country" : "Uzbekistan",
            "city" : "Tashkent",
            "email" : "andres.gomilov@hotmail.com",
            "telNumber" : "+ 998 33 746 781",
            "oxygen" : 93,
            "bSugar" : 6.0,
            "postal" : 3650,
            "ageP" : 1993
            
           /* {"key": "temperature", "value": 37.4},
            {"key": "weight", "value": 79},
            {"key": "dia_bPressure", "value": 70},
            {"key": "sys_bPressure", "value": 82},
            {"key": "eduCentre", "value": "Tashkent university of Medicine"},
            {"key": "country", "value": "Uzbekistan"},
            {"key": "city", "value": "Tashkent"},
            {"key": "adress", "value": "Street of Hamir Gerbanov 23f"},
            {"key": "email", "value": "andres.gomilov@hotmail.com"},
            {"key": "telNumber", "value": "+998 33 746 781"},
            {"key": "oxygen", "value": 99},
            {"key": "bSugar", "value": 6.0}*/
        }
    },
    {
        "firstNames": "Barrack",
        "lastNames": "Trump",
        "gender": "OTHER",
        "dateOfBirth": "1959-12-29",
        "address": {
            "address": "Washington DC, United States"
        },
        "additionalInfo": {
            "graph1" : "192.3,192,192.1,192.3",
            "graph2" : "103,94,98,107",
            "graph3" : "6.0,7.3,7.6,7.5",
            "graph4" : "70,71,71.5,70",
            "graph5" : "92,91.5,95,92",

            "height" : 192.3,
            "weight" : 107,
            "temperature" : 35.3,
            "dia_bPressure" : 70,
            "sys_bPressure" : 105,
            "eduCentre" : "University of Social sciences, liberal arts degree",
            "country" : "United States",
            "city" : "Washington",
            "email" : "barrack.trump@idiot.com",
            "telNumber" : "+ 1 132 796 8781",
            "oxygen" : 92,
            "bSugar" : 7.5,
            "postal" : 1005,
            "ageP" : 1959
        }
    },
    {
        "firstNames": "Muammar",
        "lastNames": "Al-Gadafi",
        "gender": "MALE",
        "dateOfBirth": "1953-06-07",
        "address": {
            "address": "Al-Shahir, Libya"
        },
        "additionalInfo": {
            "graph1" : "171,172,170,170.8",
            "graph2" : "82,83,81,83",
            "graph3" : "6.0,5.7,4.1,4.3",
            "graph4" : "90,92,93,94",
            "graph5" : "100,96,93,96",
            
            "height" : 170.8,
            "weight" : 83,
            "temperature" : 37.4,
            "dia_bPressure" : 90,
            "sys_bPressure" : 142,
            "eduCentre" : "None",
            "country" : "Libya",
            "city" : "Al-Shahir",
            "adress" : "Square of Islamic rebellion, 4b",
            "email" : "inukedyourhome@isis.com",
            "telNumber" : "+ 138 433 746 781",
            "oxygen" : 100,
            "bSugar" : 4.3,
            "postal" : 1470,
            "ageP" : 1953
        }
    },
];

function pacientSelected(i){
    var p = pacienti[i];
    
    $("#patient-name").html(`${p.firstNames} ${p.lastNames}`);
    $("#patient-age").html(`23`);
    statusWeight(p.additionalInfo.weight);
    statusBloodSugar(p.additionalInfo.bSugar);
    statusOxygenSaturation(p.additionalInfo.oxygen);
    statusBloodPressure(p.additionalInfo.sys_bPressure);

    //$("#medication").html(JSON.stringify(pacienti[i]));
    console.log(p);
    vnasanjePodatkov(p);
    updateGraph(p);
}

// priklice podatke 


function beriZnak(){
    var elem = document.getElementById('gumbko');
    elem.addEventListener('keypress', function(e){
       if (e.keyCode == 13){
           searchPacient();
       } 
    });
    
}

//smol penis

function generirajPodatke(stPacienta) {
    pacienti.forEach(function(pacient, i){
        generirajPacienta(pacient, function(noviPacient){
            pacienti[i] = noviPacient;
            $("#pacienti").append(`<p>${JSON.stringify(noviPacient)}</p>`);
            $("#pacientek").append(`
                <a id="pacient-${i}" onclick="pacientSelected(${i})" class="dropdown-item" href="#!">${noviPacient.firstNames} ${noviPacient.lastNames}</a>
                <br>
            `);
            $("#alerts").append("<p> </p> <span class='alerts' " + "role='alert'> Generirali ste pacienta '" + pacienti[i].firstNames+
            pacienti[i].lastNames  + '"!</span>"');
            $("#alerts").fadeIn(500);
            $("#alerts").fadeOut(2000);

        });
        console.log("tralala");
    });
      ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
    
    
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function generirajPacienta(pacientInfo, cb) {
    
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
        "Authorization": getAuthorization()
            
        },
        
        success: function(data) {
            var ehrId = data.ehrId;
            console.log(pacientInfo);
            
            pacientInfo.additionalInfo.ehrId=ehrId;
            
            // build party data
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                headers: {
                     "Authorization": getAuthorization()
                },
                
                data: JSON.stringify(pacientInfo),
                success: function(party) {
                    if (party.action == 'CREATE') {
                       //useless code $("#pacientek").append("<a class='dropdown-item' href=\"#!\"> "+pacientInfo.firstNames + " " + pacientInfo.lastNames+ " " +"</a><br>");
                        cb(pacientInfo);
                    }
                }
            });
        }
    });
    
}


function searchPacient(){
    var ehrId = $("#gumbko").val();
    
    document.getElementById("fullName").innerHTML=(" -");
    document.getElementById("gender").innerHTML=(" -");
    document.getElementById("dob").innerHTML=(" -");
    document.getElementById("adress").innerHTML=(" -");
    document.getElementById("countryP").innerHTML=(" -");
    document.getElementById("eduCentre").innerHTML=(" -");
    document.getElementById("email").innerHTML=(" -");
    document.getElementById("telNumber").innerHTML=(" -");
    document.getElementById("postal").innerHTML=(" -");
    document.getElementById("city").innerHTML=(" -");
    document.getElementById("height").innerHTML=(" -");
    document.getElementById("weight").innerHTML=(" -");
    document.getElementById("bSugar").innerHTML=(" -");
    document.getElementById("diastolic").innerHTML=(" -");
    document.getElementById("systolic").innerHTML=(" -");
    document.getElementById("systolic").innerHTML=(" -");
    document.getElementById("oxygenSat").innerHTML=(" -");
    
    //zbrise age
    document.getElementById("age").innerHTML = " -";
    
    //zbrise gliphyicon
    $("#statusWeight123").removeClass();
    $("#statusBloodSugar123").removeClass();
    $("#oxygenSatGly").removeClass();
    $("allPressure123").removeClass();
    
    
    var searchData = [
        {key: "ehrId", value: ehrId}
    ];
    $.ajax({
        url: baseUrl + "/demographics/party/query",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(searchData),
        success: function (res) {
            for (i in res.parties) {
                var party = res.parties[i];
                document.getElementById("patient-name").innerHTML=(res.parties[i].firstNames + " " + res.parties[i].lastNames);
                document.getElementById("fullName").innerHTML=(res.parties[i].firstNames + " " + res.parties[i].lastNames);
                document.getElementById("gender").innerHTML=(res.parties[i].gender);
                document.getElementById("dob").innerHTML=(res.parties[i].dateOfBirth);
                document.getElementById("adress").innerHTML=(res.parties[i].address);
                console.log(res.parties[i].firstNames);

            }
        }
    });
}

function statusWeight (weightStatus){
    
    $("#statusWeight123").removeClass();
    
    if(weightStatus <= 85){
        document.getElementById('statusWeight123').className = "fas fa-check-circle colorsOK";
    }
    if(weightStatus > 85 && weightStatus < 105){
        document.getElementById('statusWeight123').className = "fas fa-info-circle colorsWatchOut";
    }
    if(weightStatus > 106){
        document.getElementById('statusWeight123').className = "fas fa-times-circle colorsBAD";
    }
    
    return weightStatus;
    
}

function statusBloodSugar (bloodSugar){
    
    $("#statusBloodSugar123").removeClass();
    
    switch(bloodSugar){
        //koda:statusBloodSugar123
        
        case 4.3:
            document.getElementById('statusBloodSugar123').className = "fas fa-check-circle colorsOK";
          break;
          
        case 6.0:
            document.getElementById('statusBloodSugar123').className = "fas fa-info-circle colorsWatchOut"
            break;
            
        case 7.5:
            document.getElementById('statusBloodSugar123').className = "fas fa-times-circle colorsBAD";
            break;
    }
    
    //to be continued
    return bloodSugar;
}

function statusOxygenSaturation(oxygenS){
    
    $("#oxygenSatGly").removeClass();
    
    switch(oxygenS){
        //koda:statusBloodSugar123
        
        case 100:
            document.getElementById('oxygenSatGly').className = "fas fa-check-circle colorsOK";
          break;
          
        case 93:
            document.getElementById('oxygenSatGly').className = "fas fa-info-circle colorsWatchOut"
            break;
            
        case 92:
            document.getElementById('oxygenSatGly').className = "fas fa-times-circle colorsBAD";
            break;
    }
    
    return oxygenSat;
    
}

function statusBloodPressure(bloodPressure){
    
    $("allPressure123").removeClass();
    
    switch(bloodPressure){
        //koda:allPressure123
        
        case 105:
            document.getElementById('allPressure123').className = "fas fa-check-circle colorsOK";
          break;
          
        case 130:
            document.getElementById('allPressure123').className = "fas fa-info-circle colorsWatchOut";
            break;
            
        case 142:
            document.getElementById('allPressure123').className = "fas fa-times-circle colorsBAD";
            break;
    }
    
    return bloodPressure;
    
}


function racunajStarost(x){
    
    var starost;
    var starost2;
    starost = 2019 - x;
    console.log(starost);
    starost2 = starost.toString();
    document.getElementById("age").innerHTML = starost2;
    return starost2;
}

function vnasanjePodatkov(k){
    document.getElementById("age").innerHTML = (" -");
    
    document.getElementById("fullName").innerHTML=(k.firstNames + " " + k.lastNames);
    document.getElementById("gender").innerHTML=(k.gender);
    document.getElementById("dob").innerHTML=(k.dateOfBirth);
    console.log(k.dateOfBirth);
    document.getElementById("adress").innerHTML=(k.address.address);
    console.log(k.additionalInfo.country);
    document.getElementById("countryP").innerHTML=(k.additionalInfo.country);
    document.getElementById("eduCentre").innerHTML=(k.additionalInfo.eduCentre);
    document.getElementById("email").innerHTML=(k.additionalInfo.email);
    document.getElementById("telNumber").innerHTML=(k.additionalInfo.telNumber);
    console.log(k.additionalInfo.telNumber);
    document.getElementById("postal").innerHTML=(k.additionalInfo.postal);
    document.getElementById("city").innerHTML=(k.additionalInfo.city);
    document.getElementById("height").innerHTML=(k.additionalInfo.height);
    document.getElementById("weight").innerHTML=(k.additionalInfo.weight);
    document.getElementById("bSugar").innerHTML=(k.additionalInfo.bSugar);
    document.getElementById("diastolic").innerHTML=(k.additionalInfo.dia_bPressure);
    document.getElementById("systolic").innerHTML=(k.additionalInfo.sys_bPressure);
    document.getElementById("oxygenSat").innerHTML=(k.additionalInfo.oxygen);
    
    racunajStarost(k.additionalInfo.ageP);
    console.log(k.firstNames);
    //statusi:
    // status weight
    statusWeight(k.additionalInfo.weight);
    
    // status blood sugar
    statusBloodSugar(k.additionalInfo.bSugar);
    
    // status oxygen saturation
    statusOxygenSaturation(k.additionalInfo.oxygen);
    
    // status blood pressure
    statusBloodPressure(k.additionalInfo.sys_bPressure);
}




window.addEventListener('load', function(){
      $("#health").fadeOut();
      $("#appointments").fadeOut();
      $("#help").fadeOut();
      $("#qa").fadeOut();
      $("#perscription").fadeOut();
     
      
      
});
window.addEventListener('load', function(){
     $("#medication").fadeOut();
});



 
function odpriMeni(izbira){
  console.log("odpri: " + izbira);
  switch(izbira){
    
    case "health":
    $("#bio").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeOut(0);
    $("#health").fadeIn(0);
      break;
      
    case "bio":
    $("#health").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeOut(0);
    $("#bio").fadeIn(0);
      break;
      
    case "appointments":
    $("#appointments").fadeIn(0);
    $("#bio").fadeOut(0);
    $("#health").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeOut(0);
      break;
    
    case "medication":
    $("#bio").fadeOut(0);
    $("#health").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeIn(0);
      break;
      
    case "qa":
    $("#bio").fadeOut(0);
    $("#health").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeIn(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeOut(0);
      break;
      
    case "help":
    $("#bio").fadeOut(0);
    $("#health").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeIn(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeOut(0);
    $("#medication").fadeOut(0);
      break;
      
    case "perscription":
    $("#bio").fadeOut(0);
    $("#health").fadeOut(0);
    $("#appointments").fadeOut(0);
    $("#help").fadeOut(0);
    $("#qa").fadeOut(0);
    $("#perscription").fadeIn(0);
    $("#medication").fadeOut(0);
      break;
  
  }
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function updateGraph(person){
    person.additionalInfo.graph1.split(',').forEach(function(str,i){
        charts[0].data[0].dataPoints[i] = {
            'label': `Height:`,
            'y': parseFloat(str)
        };
    });
    person.additionalInfo.graph2.split(',').forEach(function(str,i){
        charts[1].data[0].dataPoints[i] = {
            'label': `Weight:`,
            'y': parseFloat(str)
        };
    });
    person.additionalInfo.graph3.split(',').forEach(function(str,i){
        charts[2].data[0].dataPoints[i] = {
            'label': `BSL:`,
            'y': parseFloat(str)
        };
    });
    person.additionalInfo.graph4.split(',').forEach(function(str,i){
        charts[3].data[0].dataPoints[i] = {
            'label': `BP:`,
            'y': parseFloat(str)
        };
    });
    person.additionalInfo.graph5.split(',').forEach(function(str,i){
        charts[4].data[0].dataPoints[i] = {
            'label': `OxySat:`,
            'y': parseFloat(str)
        };
    });

    charts.forEach(function(c){
        c.render();
    });

}

function onCheckBoxClick(){
    $('#customCheck1').prop('checked', false);
    $('#customCheck2').prop('checked', false);
    $('#customCheck3').prop('checked', false);
    $('#customCheck4').prop('checked', false);
    $('#customCheck5').prop('checked', false);
}
var charts = [];

window.onload = function() {
    try{
        charts.push(new CanvasJS.Chart("chartContainer1", {
            title: { text: "Chart of height" },
            data: [
                { dataPoints: [{}] }
            ]
        }));
        charts.push(new CanvasJS.Chart("chartContainer2", {
            title: { text: "Chart of weight" },
            data: [
                { dataPoints: [{}] }
            ]
        }));
        charts.push(new CanvasJS.Chart("chartContainer3", {
            title: { text: "Chart of blood sugar level" },
            data: [
                { dataPoints: [{}] }
            ]
        }));
        charts.push(new CanvasJS.Chart("chartContainer4", {
            title: { text: "Chart of blood pressure (diastolic)" },
            data: [
                { dataPoints: [{}] }
            ]
        }));
        charts.push(new CanvasJS.Chart("chartContainer5", {

            title: { text: "Chart of oxygen saturation" },
            data: [
                { dataPoints: [{}] }
            ]
        }));
    charts.forEach(function(c)
    {c.render()});
    }
    catch(x){
       // console.log(x);
    }
};

$.getJSON('knjiznice/json/birthRateJSON.json', function(data){
    $.each(data.fact, function(i, field){
           
           
           function gettingOut(){
               var countryhex = i;
               return countryhex;
           }
           
           var country = data.fact[i];
          // var length = data.fact.length;
          // console.log(length);
          
          
           
           $("#dropdownMenus").append(`<a onclick="vnasanjeCountry(${i})" class="dropdown-item" href="#">${country.dims.COUNTRY} </a><br>`);
           
           console.log(i.toString()); //indeks
           console.log(data.fact[i].Value); //vrednost 
           console.log(data.fact[i].dims.COUNTRY); //ime drzave
           
           
           
           function vnasanjeCountry(){
               
               document.getElementById("adolValue").innerHTML=(data.fact[i].Value);
           }
           vnasanjeCountry();
    });
});

//gettingOut();
/*function countrySelected(){
    var country = data.fact[i];
}*/

//EXAMPLARY CODE:
/*$("#pacientek").append(`
                <a id="pacient-${i}" onclick="pacientSelected(${i})" class="dropdown-item" href="#!">${noviPacient.firstNames} ${noviPacient.lastNames}</a>
                <br>
            `); */